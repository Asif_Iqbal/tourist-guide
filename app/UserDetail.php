<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdetail extends Model
{

    protected $fillable = ['user_id', 'beach', 'hill', 'museum', 'temple', 'historical_place', 'water_fall', 'natural_place'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
